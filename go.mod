module gitlab.com/akki.jura96/runners-go

go 1.21.1

require (
	github.com/akijura/post05 v0.0.0-20231014171406-559a71c25cac // indirect
	github.com/lib/pq v1.10.9 // indirect
)
